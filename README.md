# Floating Islands
![crab bot](https://scontent-ort2-1.xx.fbcdn.net/v/t1.6435-9/37899359_898926856961860_511087067975909376_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=0debeb&_nc_ohc=UtwK1MWPiFcAX_3YqN4&_nc_ht=scontent-ort2-1.xx&oh=5929e6e8cc055c89bc3495d8d82d7a4f&oe=61CFA330)

The floating islands is an interactive arts display developed in conjunction with the Oakland University College of Arts and Sciences. 
Each island operates independently with its own scripts that move the elements on the island. 


## Technical Description 
Each island on the floating islands display utilizes its own arduino board, for boards with servos all interfacing is handled through a PCA9685 PWM driver. 
Neopixels are used extensively throughout the project to generate light effects. 

## Installation

This repo contains the arduino source code for the floating islands project. 
There are additional dependencies that will be required in order to compile the floating islands code in the Arduino IDE. 

[Adafruit NeoPixel](https://github.com/adafruit/Adafruit_NeoPixel)

[Adafruit Servo Driver](https://github.com/adafruit/Adafruit-PWM-Servo-Driver-Library)

## Contributing
Makers at OU welcomes all OU students to join to work on fun and interactive projects. Contact us on [grizzorgs](https://oaklandu.campuslabs.com/engage/organization/makers-at-oakland-university)
